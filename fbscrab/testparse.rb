require 'rubygems'
require 'xmlsimple'
require 'dbi'

dsn = ARGV[0]
file = ARGV[1]

$dbh = DBI.connect("DBI:sqlite3:#{dsn}", '', '')
$dbh['AutoCommit'] = false

d = XmlSimple.xml_in(open(file).readlines.join(''))

# CREATE TABLE game (id int, startDate int, endDate int, status char(12), secondsPerMove int);
# CREATE TABLE game_score (game_id int, player int, score int);
# CREATE TABLE users (id int, name varchar(64));

# get existing IDs from the table
seen_ids = {}
$dbh.select_all('select id from game').each { |row|
    seen_ids[row[0].to_i] = 1
}

new_games = 0
d['Object'][0]['Array'].each { |i|
    if i['name'] == 'complete' then
        i['Object'].each { |x|
            a = {}
            x['Number'].each { |n| a[n['name']] = n['value'] }
            a['status'] = x['String'][0]['value']

#            puts "check: #{a['game']} => #{seen_ids[a['game'].to_i]}"
            if seen_ids[a['game'].to_i].nil? then
                new_games = 1
            end

# {"status"=>"ended", "game"=>"49468976", "startDate"=>"1262446154", "secondsPerMove"=>"86400", "endDate"=>"1262810899"}
# [{"Number"=>[{"name"=>"score", "value"=>"294"}, {"name"=>"player", "value"=>"673541639"}], "Boolean"=>[{"name"=>"winner", "value"=>"false"}, {"name"=>"joined", "value"=>"true"}]}, {"Number"=>[{"name"=>"score", "value"=>"351"}, {"name"=>"player", "value"=>"761615878"}], "Boolean"=>[{"name"=>"winner", "value"=>"true"}, {"name"=>"joined", "value"=>"true"}]}]

            $dbh.transaction do
                $dbh.do(
                    "INSERT OR IGNORE INTO game VALUES (?,?,?,?,?,current_timestamp)",
                    a['game'], a['startDate'], a['endDate'], a['status'], a['secondsPerMove']
                )
	            x['Array'][0]['Object'].each { |player|
	                q = {}
	                player['Number'].each { |n| q[n['name']] = n['value'] }
                    $dbh.do("INSERT OR IGNORE INTO game_score VALUES (?,?,?)", a['game'], q['player'], q['score'])
                }
            end
        }
    end
}

if new_games == 1 then
    exit 0
else
    exit 1
end
