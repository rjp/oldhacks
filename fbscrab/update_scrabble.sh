ARCHIVE=/data/rjp/scrabble
SCRABDIR=/home/rjp/tmp
EMAILTO=zimpenfish@gmail.com

# replace 673541639 with your own number (look in scrabbles.db, users table)
wget -q -O /tmp/fbscrab.xml 'http://fb-scrabble.ghsrv.com/server.jsp?nocache=1&message=%3CMessage%20id%3D%22scrabble-facebook-3Xf0lq0T6Cw3%22%3E%3CObject%20name%3D%22MyGames%22%3E%3CNumber%20name%3D%22player%22%20value%3D%22673541639%22%2F%3E%3CNumber%20name%3D%22lastRequest%22%20value%3D%220%22%2F%3E%3CNumber%20name%3D%22network%22%20value%3D%221%22%2F%3E%3C%2FObject%3E%3C%2FMessage%3E'

if ruby testparse.rb $SCRABDIR/scrabbles.db /tmp/fbscrab.xml; then
# TODO change the paths
    mkdir -p $ARCHIVE/$(date +%Y%m%d)
    cp /tmp/fbscrab.xml $ARCHIVE/$(date +%Y%m%d)/$(date +%Y%m%d%H%M).xml
# TODO change the email address if you want the emailed output
    (echo ".header on"; echo ".mode tabs"; echo "select x as who, count(y) as wins, min(y) as min, round(avg(y),-1) as avg, max(y) as max from (select coalesce(nullif(coalesce(nullif(rjp-anne < 0,0),'rjp'),1), nullif(coalesce(nullif(rjp-anne > 0,0),'anne'),1)) as x, abs(rjp-anne) as y, rjp, anne from rjp_anne as a, game as b where b.enddate > 1270076400 and a.game_id=b.id and b.status='ended' and rjp<>anne) as z group by x;") | sqlite3 $SCRABDIR/scrabbles.db | mail -s "Latest scrabble statistics since 2010-04-01" $EMAILTO
# TODO obviously this one will need mangling and the rjp_proogs view changing
    (echo ".header on"; echo ".mode tabs"; echo "select x as who, count(y) as wins, min(y) as min, round(avg(y),-1) as avg, max(y) as max from (select coalesce(nullif(coalesce(nullif(rjp-proogs < 0,0),'rjp'),1), nullif(coalesce(nullif(rjp-proogs > 0,0),'proogs'),1)) as x, abs(rjp-proogs) as y, rjp, proogs from rjp_proogs as a, game as b where b.enddate > 1270076400 and a.game_id=b.id and b.status='ended' and rjp<>proogs) as z group by x;") | sqlite3 $SCRABDIR/scrabbles.db | mail -s "Latest scrabble statistics since 2010-04-01" $EMAILTO
fi

