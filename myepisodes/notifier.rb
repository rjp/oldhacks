require 'feedparser'
require 'feedparser/text-output'
require 'open-uri'
require 'time'

today = Time.now

url = ARGV[0]
if url.nil? then
    puts "usage: notifier.rb url"
    puts "url should point to a myepisodes RSS feed"
    exit
end

retries = 10
begin
    content = open(url).readlines.join('')
rescue
    retries = retries - 1
    if retries > 0 then
        sleep 30
        retry
    else
        puts "Sorry, could not read #{url} in 10 attempts"
        exit(1)
    end
end
rss = FeedParser::Feed::new content
downloads = []

if rss.items[0].title == 'No Episodes' then
    # no episodes so quit silently
    exit
end

rss.items.each { |i|
    i.content.gsub!(%r{</tr>}, "\n").gsub!(/<.*?>/, '')
    c = i.content.match(/Air Date:\s*(\d\d-\w{3}-\d{4})/)
    unless c.nil? and c[1].nil? then
        date = Time.parse(c[1])
        if date then
            days_old = (today - date) / 86400
            if days_old > 1 and days_old < 3 then
                downloads.push i.title
            end
        end
    end
}
if downloads.size > 0 then
    puts "These recent programs may be available for download:"
    puts
    puts downloads.join("\n")
end

