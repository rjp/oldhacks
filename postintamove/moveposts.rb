#!/usr/bin/env ruby

require "rubygems"
require "posterous"
require "pp"
require ENV['HOME'] + "/.apikeys.rb"

Posterous.config = $apikeys[:posterous]

include Posterous

from_site = Site.find('zimpenfish')
to_site = Site.find('zimpenfishtagram')

puts "from: #{from_site.id} to: #{to_site.id}"
puts "from count: #{from_site.posts_count}"

def is_instagram?(post)
    post.body_full =~ /instagr/
end

to_process = from_site.posts_count
processed = 0
page = 1

while processed < to_process
    posts = from_site.posts(:page => page)
    posts.each do |post|
        if is_instagram?(post) then
            puts "#{post.title} on #{post.site_id} is instagram'd"
            post.site_id = to_site.id
            post.save
            sleep 2
        end     
        processed = processed + 1
    end 
    page = page + 1
end

