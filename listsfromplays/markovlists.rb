class Track
    attr_accessor :plays, :links, :name

    def initialize(name)
        @plays = 0
        @links = Hash.new(0)
        @name = name
    end
end

tracks = {}
prev = nil

$stdin.readlines.each do |line|
    line.chomp!

    if tracks[line].nil? then
        a = Track.new(line)
        tracks[line] = a
    end
    b = tracks[line]
    b.plays = b.plays + 1

    if not prev.nil? then
        c = tracks[prev]
        c.links[line] = c.links[line] + 1
    end
    prev = line
end    

playlists = Hash.new{|h,k| h[k]={}}
followers = {}

tracks.sort_by {|k,v| k}.each do |key, track|
    $stderr.puts "T #{track.plays} #{track.name}"
    track.links.each do |k,v|
        r = v.to_f / track.plays.to_f
        if v > 1 then # followed more than once
            $stderr.puts "+ #{k} #{r} #{v}"
            playlists[track.name][k] = 1
            followers[k] = 1
        end
    end
end

$stderr.puts playlists.inspect
$stderr.puts followers.inspect

def subtrees(playlists, r, seen)
    a = []
    $stderr.puts "? #{r} #{playlists[r]}"

    playlists[r].keys.each do |l|
        if seen[l].nil? then
            seen[l] = 1
	        subtrees(playlists, l, seen) do |i|
	            yield [r, i.flatten]
	        end
	        yield [r, l]
        end
    end
end
        
c=0
roots = playlists.keys - followers.keys
roots.each do |r|
    a = []
    subtrees(playlists, r, {}) do |i| a.push i.flatten; end
    m = a.sort_by {|x| x.length}[-1]
    puts "PLAYLIST #{c}"
    puts m.join("\n")
    puts
    c = c + 1
end
