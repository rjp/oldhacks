require 'rubygems'
require 'hpricot'
require 'open-uri'
require 'chronic'
require 'icalendar'

t = Chronic.parse("this month 1")
forwards = 8 # how many months to download

ics = Icalendar::Calendar.new

loop {
	today = "#{t.year}_#{t.month}"
#    puts "fetching listings for #{today}"
	doc = Hpricot(open("http://www.stargreen.com/listing/#{today}"))
	
	curdate = 'MONKEY'
	
	doc.search('/html/body/table/tr/td/table/tr') { |tr|
	    td = tr.search('/td')
	    if td[0]['CLASS'] == 'title' then
	        curdate = Time.parse(td[0].inner_text.chomp)
	    elsif td.size == 4 then
	        (who, where, price, junk) = td.map{|x| x.inner_text}
	        junk.gsub!(/[^A-Za-z0-9 ]/,'')
#puts "#{curdate.strftime('%Y%m%d')},#{who},#{where},#{price},#{junk}"
            event = Icalendar::Event.new
            event.start = curdate.strftime('%Y%m%dT000000')
            event.end = curdate.strftime('%Y%m%dT235959')
            event.summary = who
            event.description = "#{who}, #{where}, #{price}"
            event.location = where
            ics.add event
	    end
	}

    t = Chronic.parse("next month 1", :now => t)

    forwards = forwards - 1
    if forwards == 0 then
        break
    end

    sleep 15
}

ics.publish
puts ics.to_ical
