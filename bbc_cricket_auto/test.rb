require 'rubygems'
require 'hpricot'
require 'open-uri'
require 'digest/sha1'
require 'gdbm'

mygdbm = GDBM.new("cricketfeed.db")

livetext = nil
url = ARGV[0]
lim = -1

if ARGV[0].nil? then
    loop do
		doc = Hpricot(open('http://news.bbc.co.uk/sport'))
		livetext = doc.search('//a[text()*="Live text -"]')
        if livetext.size > 0 then 
		    url = 'http://news.bbc.co.uk' << livetext[0]['href']
            break
        end
        now = Time.now
        offset = 3600 - ( 60 * now.min + now.sec )
        puts "#{Time.now}: no link, sleeping for #{offset} (#{offset/60}:#{offset%60})"
        sleep offset
    end
else
    url = ARGV[0]
    livetext = [1]
    lim = ARGV[1].to_i
end

if livetext.size > 1 then
    puts "more than one livetext URL, sorry, supply it by hand"
    exit
end

started = nil

loop do
	doc = Hpricot(open(url))
    paragraphs = []
	
#doc.search('//span/b[text()*="tms@"]').each {|x|
	doc.search('//span/b[text()*="81111"]').each {|x|
	    para = x.parent
	    y = para.parent
	    while y = y.next_sibling do
	        text = y.inner_text.sub(%r{(?m:Bookmark.*$)},'').strip
            sha1 = Digest::SHA1.hexdigest(text)
            if mygdbm[sha1].nil? then
                paragraphs << text
                mygdbm[sha1] = 'yes'
            end
	    end
	}
    if started.nil? then # first pass
    puts "limiting to #{lim} paragraphs"
        paragraphs = paragraphs[0..lim]
    end
    started = true
    paragraphs.reverse.each { |p|
        color = 0
        if p =~ /^\d{4}/ then color = 33; end
        if p =~ /^"/ then color = 32; end
        if p =~ /WICKET/ then color = "1;31"; end
        puts "[#{color}m#{p}[0m\n"
        sleep 2
    }
    sleep 40
end
